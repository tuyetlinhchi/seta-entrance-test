import {useEffect, useState} from "react";
import Modal from "./component/Modal";
import {AddPost} from "./component/AddPost";
import Post from "./component/Post";

function App() {
  const [posts, setPosts] = useState();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const getPosts = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts");
    const posts = await response.json();
    setPosts(posts);
  };

  useEffect(() => {
    getPosts();
  }, []);

  const handleCloseModal = () => {
    setOpen(false);
  };

  const addPost = async (title, body) => {
    try {
      setLoading(true);
      const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
          title: title,
          body: body,
          userId: 1,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      console.log("click");

      if (!response.ok) {
        throw new Error("Failed to add post");
      }

      const data = await response.json();
      setPosts((posts) => [data, ...posts]);
      setLoading(false);
      setOpen(false);
    } catch (error) {
      console.error("Error adding post:", error);
    }
  };

  return (
    <div className="p-10">
      <button
        className="bg-zinc-300 px-3 py-1 rounded-lg hover:opacity-60 mb-3 min-w-[100px]"
        onClick={() => setOpen(true)}
      >
        Create post
      </button>
      <div className="grid grid-cols-4 gap-4">
        {posts?.map((post) => (
          <Post post={post} key={post.title} />
        ))}
      </div>
      <Modal title="Add Post" show={open} handleClose={handleCloseModal}>
        <AddPost onAdd={addPost} loading={loading} />
      </Modal>
    </div>
  );
}

export default App;
