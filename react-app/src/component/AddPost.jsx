export const AddPost = ({onAdd, loading}) => {
  const handleOnSubmit = (e) => {
    e.preventDefault();
    onAdd(e.target.title.value, e.target.body.value);
    e.target.title.value = "";
    e.target.body.value = "";
  };

  return (
    <form onSubmit={handleOnSubmit}>
      <input className="w-[40%] rounded-lg px-2 mr-2 border-2" placeholder="Title" name="title" />
      <input className="w-[40%] rounded-lg px-2 mr-2 border-2" placeholder="Body" name="body" />
      <button
        className="bg-blue-300 px-3 py-1 rounded-lg hover:opacity-60 min-w-[100px]"
        onSubmit={handleOnSubmit}
      >
        {loading ? " loadding ..." : "Add"}
      </button>
    </form>
  );
};
