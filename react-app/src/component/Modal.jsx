const Modal = ({handleClose, show, title, children}) => {
  return (
    <div className={show ? "block" : "hidden"}>
      <div
        onClick={handleClose}
        className={`fixed top-0 left-0 w-full h-full bg-black opacity-80`}
      ></div>
      <section className="fixed bg-white !opacity-100 w-[80%] h-auto top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 p-5">
        <div className="flex justify-between">
          <h3>{title}</h3>
          <button className="p-2 " type="button" onClick={handleClose}>
            X
          </button>
        </div>
        {children}
      </section>
    </div>
  );
};
export default Modal;
