const Post = ({post}) => {
  return (
    <div className="col-span-1 shadow-md p-4">
      <div className="text-xl truncate">{post.title}</div>
      <div className="">{post.body}</div>
    </div>
  );
};
export default Post;
