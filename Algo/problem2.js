const sumOfTopTwoFirstSolution = (arr) => {
  if (!Array.isArray(arr)) return "param must be an array";
  if (arr.length < 2) return "param must have at least 2 element";
  if (arr.forEach((element) => typeof element !== "number"))
    return "every element of array must be the number";

  arr.sort((num1, num2) => num2 - num1); // sort default by desc take O(n^2) complexity

  return arr[0] + arr[1];
};

const sumOfTopTwoSecondSolution = (arr) => {
  if (!Array.isArray(arr)) return "param must be an array";
  if (arr.length < 2) return "param must have at least 2 element";
  if (arr.forEach((element) => typeof element !== "number"))
    return "every element of array must be the number";

  let maxNum1;
  let maxNum2;
  arr[0] > arr[1]
    ? ((maxNum1 = arr[0]), (maxNum2 = arr[1]))
    : ((maxNum1 = arr[1]), (maxNum2 = arr[0]));

  // loop 1 time take O(n) complexity
  for (let i = 2; i < arr.length; ++i) {
    if (arr[i] > maxNum1) {
      maxNum2 = maxNum1;
      maxNum1 = arr[i];
    }
  }
  return maxNum1 + maxNum2;
};

const compare = (element1, element2) => {
  return JSON.stringify(element1) === JSON.stringify(element2);
};

const mockTestSumOf2GreatestElement = (() => {
  const testCases = [
    {
      test: {},
      expect: "param must be an array",
    },
    {
      test: [],
      expect: "param must have at least 2 element",
    },
    {
      test: ["hello", 1],
      expect: "every element of array must be the number",
    },
    {
      test: [1, 4, 2, 3, 5],
      expect: 9,
    },
    {
      test: [-1, -2, -3, -5, 0],
      expect: -1,
    },
    {
      test: [-1, -2, -3, -5, 100, 200, 10000],
      expect: 10200,
    },
  ];

  testCases.forEach((testCase, index) => {
    const expect = testCase.expect;
    const resultOfFirstSolution = sumOfTopTwoFirstSolution(testCase.test);
    const resultOfSecondSolution = sumOfTopTwoSecondSolution(testCase.test);
    console.log(
      `Test ${index + 1}: \nInput: ${
        testCase.test
      }, expect: ${expect}, result1: ${resultOfFirstSolution}, result2: ${resultOfSecondSolution}`,
      compare(expect, resultOfFirstSolution) ? "Test1 pass" : "Test1 false",
      compare(expect, resultOfSecondSolution) ? "Test2 pass" : "Test2 false"
    );
  });
})();
