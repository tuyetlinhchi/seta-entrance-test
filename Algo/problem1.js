const mostAppearSameWordLength = (words) => {
  if (!Array.isArray(words)) return "word must be an array";
  if (!words.length) return [];
  const lengthWordsCountObj = {};

  words.forEach((word) => {
    const wordLength = word.length;
    if (!lengthWordsCountObj[wordLength]) {
      lengthWordsCountObj[wordLength] = [word];
      return;
    }
    lengthWordsCountObj[wordLength].push(word);
  });

  let mostSameWordLengthCount = 0;
  Object.values(lengthWordsCountObj).forEach((sameWordLengthArr) => {
    const wordLength = sameWordLengthArr.length;
    if (wordLength > mostSameWordLengthCount) mostSameWordLengthCount = wordLength;
  });

  let mostAppearSameLengthArr = [];
  Object.values(lengthWordsCountObj).forEach((sameWordLengthArr) => {
    if (sameWordLengthArr.length === mostSameWordLengthCount)
      mostAppearSameLengthArr = mostAppearSameLengthArr.concat(sameWordLengthArr);
  });

  return mostAppearSameLengthArr;
};

const compare = (element1, element2) => {
  return JSON.stringify(element1) === JSON.stringify(element2);
};

const mockTestMostAppearSameWordLength = (() => {
  const testCases = [
    {
      test: {},
      expect: "word must be an array",
    },
    {
      test: [],
      expect: [],
    },
    {
      test: ["hello", "world"],
      expect: ["hello", "world"],
    },
    {
      test: ["a", "ab", "abc", "cd", "gh"],
      expect: ["ab", "cd", "gh"],
    },
    {
      test: ["ab", "aac", "abc", "cd", "def", "gh"],
      expect: ["ab", "cd", "gh", "aac", "abc", "def"],
    },
    {
      test: ["a", "aa", "b", "abv", "c", "abc", "cd", "def", "gh"],
      expect: ["a", "b", "c", "aa", "cd", "gh", "abv", "abc", "def"],
    },
  ];

  testCases.forEach((testCase, index) => {
    const expect = testCase.expect;
    const result = mostAppearSameWordLength(testCase.test);
    console.log(
      `Test ${index + 1}: \nInput: [${testCase.test}], expect: [${expect}], result: [${result}]`,
      compare(expect, result) ? "Test pass" : "Test false"
    );
  });
})();
