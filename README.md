# JavaScript Algorithms

This repository contains JavaScript functions to solve specific problems related to arrays of strings and integers. Each function is accompanied by unit tests to ensure correctness.

## Table of Contents

- [Description](#description)
- [Functions](#functions)
  - [mostAppearSameWord](#mostappearsameword)
  - [sumOfTopTwoFirstSolution](#sumoftoptwofirstsolution)
  - [sumOfTopTwoSecondSolution](#sumoftoptwosecondsolution)

## Description

### Problem 1: Finding Strings with the Most Frequent Lengths

Provide an array of strings, eg:`['a', 'ab', 'abc', 'cd', 'def', 'gh']`, Write a function to find the strings’ length that appear most in this array. Writing the unit test function and provide some test-cases. The result for example array is `['ab', 'cd', 'gh']`.

### Problem 2: Finding Sum of Top 2 Integers

Provide an array of integers, eg:`[1, 4, 2, 3, 5]`,Write a function to find sum of integers on top 2. Writing the unit test function and provide some test-cases. The result for the example array is `9`.

## Functions

### mostAppearSameWord

This function takes an array of strings and returns an array of strings that have the most frequent lengths.

```javascript
const mostAppearSameWordLength = (words) => {
  if (!words.length) return [];
  
  const lengthWordsCount = {};

  // Step 1: Populate the object with word lengths as keys and arrays of words as values
  words.forEach((word) => {
    const wordLength = word.length;
    if (!lengthWordsCountObj[wordLength]) { // check if object have not wordLength key, add first word to array
      lengthWordsCountObj[wordLength] = [word];
      return;
    }
    lengthWordsCountObj[wordLength].push(word); // if object have wordLength push word to array
  });

  // Step 2: Find the maximum count of words with the same length
  let mostSameWordLengthCount = 0;
  Object.values(lengthWordsCountObj).forEach((sameWordLengthArr) => {
    const wordLength = sameWordLengthArr.length;
    if (wordLength > mostSameWordLengthCount) mostSameWordLengthCount = wordLength;
  });

  // Step 3: Collect all words with lengths that appear the most frequently
  let mostAppearSameLengthArr = [];
  Object.values(lengthWordsCountObj).forEach((sameWordLengthArr) => {
    if (sameWordLengthArr.length === mostSameWordLengthCount)
      mostAppearSameLengthArr = mostAppearSameLengthArr.concat(sameWordLengthArr);
  });

  return mostAppearSameLengthArr;
};
```

# Sum of Top Two Elements in an Array

### `sumOfTopTwoFirstSolution`

This function sorts the array in descending order and then sums the first two elements.

#### Code

```javascript
const sumOfTopTwoFirstSolution = (arr) => {
  // Validate param
  if (!Array.isArray(arr)) return "param must be an array";
  if (arr.length < 2) return "param must have at least 2 elements";
  if (arr.some((element) => typeof element !== "number"))
    return "every element of array must be a number";

  arr.sort((num1, num2) => num2 - num1); // Sort default by desc take O(n log n) complexity

  return arr[0] + arr[1];
```

### `sumOfTopTwoSecondSolution`

The second solution iterates through the array to find the two largest numbers

#### Code
```javascript
const sumOfTopTwoSecondSolution = (arr) => {
  // Validate param
  if (!Array.isArray(arr)) return "param must be an array";
  if (arr.length < 2) return "param must have at least 2 element";
  if (arr.forEach((element) => typeof element !== "number"))
    return "every element of array must be the number";

  let maxNum1;
  let maxNum2;
  arr[0] > arr[1]
    ? ((maxNum1 = arr[0]), (maxNum2 = arr[1]))
    : ((maxNum1 = arr[1]), (maxNum2 = arr[0]));

  // loop 1 time take O(n) complexity
  for (let i = 2; i < arr.length; ++i) {
    if (arr[i] > maxNum1) {
      maxNum2 = maxNum1;
      maxNum1 = arr[i];
    }
  }
  return maxNum1 + maxNum2;
};
```